#include <utility>
#include <iostream>
#include "bst.h"

using namespace std;

// function implementations for BST

// inserting
// returns:
//  true if no duplicate
//  false if duplicate
bool BST::insert(int key)
{
  if(root != NULL)
    return insert(key, root);
  else {
    root = new Node();
    root->key = key;
    root->left = NULL;
    root->right = NULL;    
    return true;
  }
}
// private version
bool BST::insert(int key, Node *leaf) {
    if(key < leaf->key) {
		if(leaf->left != NULL)
			insert(key, leaf->left);
		else {
		  leaf->left = new Node();
		  leaf->left->key = key;
		  leaf->left->left = NULL;    //Sets the left child of the child node to null
		  leaf->left->right = NULL;   //Sets the right child of the child node to null
		}  
	}
	else if(key >= leaf->key) {
		if(leaf->right != NULL)
			insert(key, leaf->right);
		else {
		  leaf->right = new Node();
		  leaf->right->key = key;
		  leaf->right->left = NULL;  //Sets the left child of the child node to null
		  leaf->right->right = NULL; //Sets the right child of the child node to null
		}
	} else
		return false; // duplicate value
	
	nodes++;
	return true;
}


// whole tree deletion
void BST::destroy()
{
  deleteTree(root);
}
// private interface for blind deletion
void BST::deleteTree(Node *leaf)
{
  if(leaf != NULL) {
    deleteTree(leaf->left);
    deleteTree(leaf->right);
    delete leaf;
  }
}


// finding nodes
Node* BST::find(int key)
{
  return find(key, root);
}
// private implementation to do work
Node* BST::find(int key, Node *leaf)
{
  if(leaf != NULL) {
    if(key == leaf->key) {
      cout << "found: " << leaf->key << endl;
      return leaf;
    }
    if(key < leaf->key && leaf->left != NULL)
      return find(key, leaf->left);
    else if(key > leaf->key && leaf->right != NULL)
      return find(key, leaf->right);
    else {
      cout << "not found!" << endl;
      return NULL;      
    }
  } else {
    return NULL;
  }
}


// preorder traversal
void BST::preorder()
{
	cout << "\t";
	preorder(root);
	cout << endl;
}
//0 private workhorse
void BST::preorder(Node *leaf)
{
  if (leaf != NULL) {
    cout << leaf->key << "    bf: " << leaf->bf << "\n\t";
    preorder(leaf->left);
    preorder(leaf->right);
  }
}


// removing a singular node
bool BST::remove(int key)
{
	Node *c;
	Node *tc;
	bool found = false;

	if (root == NULL) return false;
	else {
		c = root;
		tc = root;

		while (c != NULL && !found) {
			if (c->key == key) found = true;
			else {
				tc = c;
				if (c->key > key) c = c->left;
				else c = c->right;
			}
		}
		if (c == NULL) return false;
		else if (found) {
			if (c == root)
				remove(root);
			else if (tc->key > key)
				remove(tc->left);
			else
				remove(tc->right);
		} else
			return false;
	}		
}
// private worker
bool BST::remove(Node* &leaf) {
	Node *c;
	Node *tc;
	Node *t;

	if (leaf == NULL) return false;
	else if (leaf->left == NULL && leaf->right == NULL) {
		t = leaf;
		leaf = NULL;
		delete t;
	} else if (leaf->left == NULL) {
		t = leaf;
		leaf = t->right;
		delete t;
	} else if (leaf->left == NULL) {
		t = leaf;
		leaf = t->left;
		delete t;
	} else {
		c = leaf->right;
		tc = NULL;

		while (c->left != NULL) {
			tc = c;
			c = c->left;
		}

		leaf->key = c->key;

		if (tc == NULL)
			leaf->left = c->left;
		else
			tc->left = c->left;

		delete c;
	}	
}

// calculate balance factors
void BST::calcBF()
{
  setBF(root);
}

int BST::setBF(Node *leaf)
{
  if(leaf == NULL)
    return 0;
  if(leaf->left == NULL && leaf->right == NULL)
    return leaf->bf = 0;
  else {
    leaf->bf = height(leaf->right) - height(leaf->left);
    setBF(leaf->left);
    setBF(leaf->right);
  }
}

int BST::height(Node *leaf)
{
  if(leaf == NULL)
    return -1;
  int h = (height(leaf->left) > height(leaf->right)) ? (height(leaf->left) + 1) : (height(leaf->right) + 1);
  return leaf->h = h;
}
