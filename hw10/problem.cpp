#include <iostream>
#include "bst.h"

using namespace std;

// main 
int main(int argc, char * argv[])
{
	BST tree;
	
	tree.insert(10);
	tree.insert(4);
	tree.insert(3);
	tree.insert(12);
	tree.insert(6);
	tree.insert(5);
	tree.insert(7);
	tree.insert(8);
	
	cout << "Fresh Tree:" << endl;
	tree.preorder();
	
	tree.find(7);
	tree.find(9);
	if(tree.remove(4))
	  cout << "removed 4!";

	cout << "New Tree:" << endl;
	tree.preorder();

	cout << endl;
	cout << "With Balance Factors:" << endl;
	tree.calcBF();
	tree.preorder();
	
	return 0;
}
