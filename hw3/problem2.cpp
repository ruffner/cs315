#include <iostream>
#include <vector>

using namespace std;

// main algorithm
vector<int> mx_in_2k(vector<int> one, vector<int> two, int k);

// helpers
vector<int> nbd_in_2k(vector<int> in, int digit, int base);
void print(vector<int> larger, vector<int> smaller, int k);


int main(int argc, char * argv[]) {
  
  vector<int> k3_1_1;
  vector<int> k3_1_2 = {4,5,0,0,0,0,0,0,4,5};

  vector<int> k4_1_1 = {1};
  vector<int> k4_1_2 = {4,5,0,0,7,12,0,0,4,15};

  vector<int> k3_2_1 = {5, 5, 3, 2, 2, 2, 3, 1, 3, 2, 0, 2};
  vector<int> k3_2_2 = {0, 3, 4, 5, 0, 0, 0, 0, 5, 5, 5, 6};

  vector<int> k1_1_1 = {1, 1, 0, 1, 1, 1, 1, 1, 1};
  vector<int> k1_1_2 = {0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1};

  vector<int> k1_2_1 = {1,1,1,1};
  vector<int> k1_2_2 = {1,1,0,1};

  print(k3_1_1, k3_1_2, 3);
  print(k4_1_2, k4_1_1, 4);
  print(k3_2_1, k3_2_2, 3);
  print(k1_1_1, k1_1_2, 1);
  
  cout << endl;

  print(k1_2_1, k1_2_2, 1);
  

  return 0;
}

vector<int> mx_in_2k(vector<int> one, vector<int> two, int k) {
  if(!one.size())
    return one;
  if(!two.size())
    return two;

  vector<int> out, temp;
  int base = 1;

  for( int i = 0; i < k; i++ )
    base *= 2;

  for( int i = 0; i < one.size() + two.size(); i++ )
    out.push_back(0);

  for( int i = 0; i < two.size(); i++ ) {
    temp = nbd_in_2k(one, two[i], base);
    for(int j = 0; j < i; j++)
      temp.emplace(temp.begin(), 0);
    
    for(int j = 0; j < temp.size(); j++) {
      out[j] += temp[j];
      if(out[j] >= base) {
	out[j+1] += out[j] / base;
	out[j] %= base;
      }
    }
    
  }

  while(out.back() == 0)
    out.pop_back();
  
  return out;
}

vector<int> nbd_in_2k(vector<int> in, int digit, int base) {
  vector<int> out;
  // at most one more place
  for( int i = 0; i < in.size(); i++ )
    out.push_back(in[i]);
  out.push_back(0);

  for( int i = 0; i < out.size(); i++ ) {
    out[i] *= digit;
    if(out[i] >= base) {
      out[i+1] += out[i] / base;
      out[i] %= base;
    }
  }

  return out;
}

void print(vector<int> larger, vector<int> smaller, int k) {
	vector<int> out;
	
	if(!larger.size())
	  cout << "0";
	else
	  for( int i = larger.size()-1; i >= 0; i-- )
	    cout << larger[i] << (!i ? "" : ",");
	
	cout << " x ";
	if(!smaller.size())
	  cout << "0";
	else
	  for( int i = smaller.size()-1; i >= 0; i-- )
	    cout << smaller[i] << (!i ? "" : ",");
			
	cout << " = ";
	out = mx_in_2k(larger, smaller, k);
	
	if(!out.size())
	  cout << "0";
	else
	  for( int i = out.size()-1; i >= 0; i-- )
	    cout << out[i] << (!i ? "" : ",");
	
	cout << endl;
}
