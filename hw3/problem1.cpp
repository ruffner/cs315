#include <iostream>
#include <vector>

using namespace std;

vector<int> sub_in_2k(vector<int> larger, vector<int> smaller, int k);
void print(vector<int> larger, vector<int> smaller, int k);

int main(int argc, char * argv[]) {

	vector<int> k3_1_1;
	vector<int> k3_1_2 = {4,5,0,0,0,0,0,0,4,5};
	
	vector<int> k3_2_1 = {5,5,3,2,2,2,3,1,3,2,0,2};
	vector<int> k3_2_2 = {0,3,4,5,0,0,0,0,5,5,5,6};
	
	vector<int> k1_1_1 = {1,1,0,1,1,1,1,1,1};
	vector<int> k1_1_2 = {0,1,0,1,1,0,0,0,1,1,1,1};
	
	
	vector<int> k4_1_1 = {12,12,4,5,8,11,5,15,9,0,12};
	vector<int> k4_1_2 = {3,1,1,1,0,3,4,14,15,11,15,13};
	
	print(k3_1_2, k3_1_1, 3);
	print(k3_2_2, k3_2_1, 3);
	print(k1_1_2, k1_1_1, 1);
	print(k4_1_2, k4_1_1, 4);
	
	
	
	return 0;
}

vector<int> sub_in_2k(vector<int> larger, vector<int> smaller, int k) {
	if(!smaller.size())
		return larger;
	
	vector<int> out;
	bool borrow = false;
	int base = 1;
	for( int i = 0; i < k; i++ )
		base *= 2;
	
	for( int i = 0; i < larger.size(); i++ )
		out.push_back(0);

	for( int i = 0; i < larger.size(); i++ ) {
		if(borrow) {
			larger[i]--;
			if(larger[i] < 0) {
				borrow = true;
				larger[i] += base;
			} else {
				borrow = false;
			}
		}
				
		if(i >= smaller.size())
			out[i] = larger[i];
		else
			out[i] = larger[i] - smaller[i];
		
		if( out[i] < 0 ) {
			borrow = true;
			out[i] += base;
		} 
	}

	return out;
}

void print(vector<int> larger, vector<int> smaller, int k) {
	vector<int> out;
	
	if(!larger.size())
		cout << "0";
	else
		for( int i = larger.size()-1; i >= 0; i-- )
			cout << larger[i] << (!i ? "" : ",");
	
	cout << " - ";
	if(!smaller.size())
		cout << "0";
	else
		for( int i = smaller.size()-1; i >= 0; i-- )
			cout << smaller[i] << (!i ? "" : ",");
			
	cout << " = ";
	out = sub_in_2k(larger, smaller, k);
	
	for( int i = out.size()-1; i >= 0; i-- )
		cout << out[i] << (!i ? "" : ",");
	
	cout << endl;
}


