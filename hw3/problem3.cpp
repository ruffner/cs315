#include <iostream>
#include <vector>
#include <utility>

using namespace std;

// main algorithm
pair<vector<int>,vector<int>> div_in_2(vector<int> one, vector<int> two);

// helpers
vector<int> sub_in_2k(vector<int> larger, vector<int> smaller, int k);
void print(vector<int> larger, vector<int> smaller);


int main(int argc, char * argv[]) {

  vector<int> k1_1_1 = {0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1};
  vector<int> k1_1_2 = {0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1};

  vector<int> k1_2_1 = {0,1,1,1,1,0,0,1,1,1,0,1};
  vector<int> k1_2_2 = {0,1,0,1,1};

  vector<int> k1_3_1 = {0,1,1,1,0,1,0,1,0,0,1,1,0,1,0,1,1,0,1,1,0,1,0,1,1,0,1,1,0,0,1,1,0,0,1,1,1,0,1};
  vector<int> k1_3_2 = {0,1,0,1,1,1,1,1,1,0,0,1};

  print(k1_1_1, k1_1_2);
  print(k1_2_1, k1_2_2);
  print(k1_3_1, k1_3_2);

  return 0;
}

pair<vector<int>,vector<int>> div_in_2(vector<int> num, vector<int> denom) {
  if(!num.size())
    return make_pair(num, num);
  if(!denom.size()) {
    cout << "error: divide by zero" << endl;
    return make_pair(denom, denom);
  }

  vector<int> q, r;
  
  for( int i = 0; i < num.size()-denom.size()+1; i++ )
    q.push_back(0);

  for( int i = num.size()-1; i >= 0; i-- ) {
    r.insert(r.begin(), 0);
    while(r.back() == 0 && r.size() > 1)
      r.pop_back();

    r[0] = num[i];
    if(r.size() >= denom.size()) {
      if(r.size() == denom.size()) {
	bool go = true;
	int j = r.size()-1;
	while(go && j >= 0) 
	  if(r[j] < denom[j])
	    go = false;
	  else
	    j--;

	if(!go)
	  continue;
      }
      
      r = sub_in_2k(r, denom, 1);
      q[i] = 1;
    } 
  }

  while(!q.back() && q.size() > 1)
    q.pop_back();
  while(!r.back() && r.size() > 1)
    r.pop_back();

  return make_pair(q,r);
}

vector<int> sub_in_2k(vector<int> larger, vector<int> smaller, int k) {
	if(!smaller.size())
		return larger;
	
	vector<int> out;
	bool borrow = false;
	int base = 1;
	for( int i = 0; i < k; i++ )
		base *= 2;
	
	for( int i = 0; i < larger.size(); i++ )
		out.push_back(0);

	for( int i = 0; i < larger.size(); i++ ) {
		if(borrow) {
			larger[i]--;
			if(larger[i] < 0) {
				borrow = true;
				larger[i] += base;
			} else {
				borrow = false;
			}
		}
				
		if(i >= smaller.size())
			out[i] = larger[i];
		else
			out[i] = larger[i] - smaller[i];
		
		if( out[i] < 0 ) {
			borrow = true;
			out[i] += base;
		} 
	}

	return out;
}

void print(vector<int> larger, vector<int> smaller) {
  pair<vector<int>, vector<int>> out;
  
  if(!larger.size())
    cout << "0";
  else
    for( int i = larger.size()-1; i >= 0; i-- )
      cout << larger[i] << (!i ? "" : ",");
  
  cout << " / ";
  if(!smaller.size())
    cout << "0";
  else
    for( int i = smaller.size()-1; i >= 0; i-- )
      cout << smaller[i] << (!i ? "" : ",");
  
  cout << " = ";
  out = div_in_2(larger, smaller);
  
  for( int i = out.first.size()-1; i >= 0; i-- )
    cout << out.first[i] << (!i ? "" : ",");
  
  cout << " r: ";
  
  for( int i = out.second.size()-1; i >= 0; i-- )
    cout << out.second[i] << (!i ? "" : ",");
  
  cout << endl;
}
