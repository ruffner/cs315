#include <iostream>
#include <vector>
#include <random>
#include <chrono>
#include <string>
#include <cstdlib>
#include <list>

using namespace std;

#define ARRAYIFY 1

void randomize(vector<int> & in, int rmax);
void quick_sort(vector<int> & in);
void merge_sort(vector<int> & in);
void inser_sort(vector<int> & in);
void print(vector<int> num, bool cr);

int main(int argc, char * argv[])
{
  // misc. variables
  int size, rmax = 0;
  string in;

  // parameter definition and user input
  cout << "array size: ";
  cin >> size;
  cin.get();
  cout << "rand max (" << size << "): ";
  getline(cin, in);
  if(!in.length())
    rmax = size;
  else
    rmax = atoi(in.c_str());
  cout << endl;

  // vector creation
  vector<int> nums (size, 0);  
  

  // insertion sort demonstration
  randomize(nums, rmax);
  cout << "insertion sort" << endl;
  cout << "  before sort:\n    ";
  print(nums, true);
  inser_sort(nums);
  cout << "  after sort:\n    ";
  print(nums, true);

  

  // merge sort demonstration
  randomize(nums, rmax);
  cout << "merge sort" << endl;
  cout << "  before sort:\n    ";
  print(nums, true);
  merge_sort(nums);
  cout << "  after sort:\n    ";
  print(nums, true);


  // quick sort demonstration
  randomize(nums, rmax);
  cout << "quick sort" << endl;
  cout << "  before sort:\n    ";
  print(nums, true);
  quick_sort(nums);
  cout << "  after sort:\n    ";
  print(nums, true);

  return 0;
}

// quick sort
void quick_sort(vector<int> & in)
{
  if(in.size() <= 50) {
    inser_sort(in);
    return;
  }

  unsigned seed = chrono::system_clock::now().time_since_epoch().count();
  mt19937 rand (seed);  

  int t;

  for(int i = 0; i < in.size(); i++) {
    int pivot = rand() % in.size();
    
    t = in[in.size()-1];



  }


    advance(cmpit, pivot);
    bool go = true;

    for(int i = 0; i < l.) {
      if(--it > it)
	go = false;
      if(it < *cmpit)
	lt.push_back(it);
      else if(it > *cmpit)
	gt.push_back(it);
      else
	et.push_back(it);
      
    }
    l.clear();
    l.splice(l.begin(), lt);
    l.splice(l.end(), et);
    l.splice(l.end(), gt);
    
    if(go)
      break;
  }

  int i = 0;
  for(auto it : l)
    in[i++] = it;
}

// merge sort
void merge_sort(vector<int> & in)
{
  list<list<int>> q;
  
  for(int i = 0; i < in.size(); i++)
    q.push_back(*(new list<int> (1, in[i])));

  while(q.size() > 1) {
    list<int> a = q.front();
    q.pop_front();
    list<int> b = q.front();
    q.pop_front();
    list<int> t;
    int tot = a.size() + b.size();

    while(t.size() < tot) {
      if(a.size() && b.size()) {
	if(a.front() < b.front()) {
	  t.push_back(a.front());
	  a.pop_front();
	} else {
	  t.push_back(b.front());
	  b.pop_front();
	}
      } else if (a.size()) {
	for(auto it : a)
	  t.push_back(it);
      } else {
	for(auto it : b)
	  t.push_back(it);
      }
    }

    q.push_back(t);    
  }

  int i = 0;
  for(auto & it : q.front())
    in[i++] = it;
}

// insertion sort
// worst case O(n^2)
void inser_sort(vector<int> & in)
{
  int i, k, x;

  for(i = 1; i < in.size(); i++) {
    k = i - 1;
    x = in[i];
    while(x < in[k]) {
      in[k+1] = in[k];
      k--;
    }
    in[k+1] = x;
  }
}

// function to randomize the vector 
// based on max rand val
void randomize(vector<int> & in, int rmax)
{
  unsigned seed = chrono::system_clock::now().time_since_epoch().count();
  mt19937 rand (seed);

  for(auto & it : in)
    it = rand() % rmax;
}

// simple print helper, can print as array or just
// number based on #define ARRAYIFY at top
void print(vector<int> num, bool cr) {
  if(!num.size())
    cout << "[0]";
  for(int i = 0; i < num.size(); i++)
    if(ARRAYIFY)
      cout << (i == 0 ? "[" : ",") << num[i] << (i == num.size()-1 ? "]" : "");
    else
      cout << num[i];
  if(cr)
    cout << endl;
}
