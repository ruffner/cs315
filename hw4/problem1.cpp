#include <iostream>
#include <vector>
#include <random>
#include <chrono>
#include <utility>
#include <cmath>
#include <time.h>

#define ARRAYIFY 0

using namespace std;

vector<int> n_bit_int(int n);
pair<vector<int>,vector<int>> bin_div(vector<int> num, vector<int> denom);
vector<int> bin_mx(vector<int> one, vector<int> two);
vector<int> mod_exp_N(vector<int> x, vector<int> y, vector<int> N);
vector<int> bin_sub(vector<int> larger, vector<int> smaller);
bool is_prime(vector<int> num);
bool gtet(vector<int> larger, vector<int> smaller);
vector<int> bin2dec(vector<int> b2);
vector<int> b10x2(vector<int> in);
void print(vector<int> larger, bool cr);

int main(int argc, char * argv[]) {

  vector<int> one = n_bit_int(5);
  vector<int> two = n_bit_int(4);
  vector<int> N = n_bit_int(4);

  cout << "one: ";  print(one, false); cout << " = ";  print(bin2dec(one), true);
  cout << "two: ";  print(two, false);  cout << " = ";  print(bin2dec(two), true);
  cout << "N: ";  print(N, false); cout << " = "; print(bin2dec(N), true);


  cout << "random string generation test:\n";
  for( int i = 0; i < 15; i++ )
     print(n_bit_int(16), true);
  cout << endl;


  cout << "bin_mx(one, two): ";  print(bin_mx(one, two), false); cout << " = "; print(bin2dec(bin_mx(one, two)), true);
  cout << "bin_mx(one, one): ";  print(bin_mx(one, one), false);  cout << " = "; print(bin2dec(bin_mx(one, one)), true);


  cout << "gtet test" << endl;
  if(gtet(one, two))
    cout << "  one >= two" << endl;
  else
    cout << "  one < two" << endl;


  // division
  cout << "bin_div(one, two) (q,r): ";  print(bin_div(one, two).first, false);
  cout << ", ";
  print(bin_div(one, two).second, false);
  cout << " = ";
  print(bin2dec(bin_div(one, two).first), false);
  cout << ", ";
  print(bin2dec(bin_div(one, two).second), true);

  // modular exponentiation
  cout << "mod_exp_N(one, two, N): ";
  print(mod_exp_N(one, two, N), false);
  cout << " = ";
  print(bin2dec(mod_exp_N(one, two, N)), true);
  
  vector<int> test = {1,1,1};
  cout << "is "; print(test, false); cout << " prime: "; cout << is_prime(test); cout << endl << endl;


  // brute force checking 100 16 bit primes
  cout << "BRUTE FORCE CHECKING" << endl;
  int c = 0;
  int t = 0;
  while(c < 100) {
    vector<int> n = n_bit_int(16);
    if(is_prime(n)) {
      print(n, false);
      cout << " = ";
      print(bin2dec(n), false);

      int p = 0;
      vector<int> p_num = bin2dec(n);
      for(int i = 0; i < p_num.size(); i++)
	p += p_num[i]*pow(10,i);
     
      int up_lim = floor(sqrt(p));
      int i;
      for(i = 2; i < up_lim; i++)
	if(!(p % i))
	  break;
      
      if(i == up_lim) {
	cout << " - PASSED" << endl;
	c++;
      } else {
	cout << " - FAILED - divisible by " << i  << endl;
      }
      t++;
    }
  }

  cout << c << "/" << t << " primes generated were prime." << endl << endl;


  cout << "GENERATING PRIMES" << endl << endl;

  for(int bits = 16; bits <= 512; bits *= 2) {
    cout << "generating 1 " << bits << " bit prime:\n";
    int count = 0;
    int total = 0;

    auto start = std::chrono::system_clock::now();

    while( count < 1 ) {
      vector<int> num = n_bit_int(bits);
      if(is_prime(num)) {
	print(num, false);
	cout << " = ";
	print(bin2dec(num), true);
	count++;
	total++;
      } else {
	total++;
      }
    }

    auto duration = chrono::duration_cast<chrono::milliseconds> (chrono::system_clock::now() - start);

    cout << "  * generation took " << (duration.count()/1000.0) << " seconds." << endl;
    cout << "  * " << (float)count/total*100 << "% success rate with " << bits << " bits" << endl;
    cout << "  * took " << total << " attempts" << endl << endl;
  }

  return 0;
}

// binary to decimal conversion
vector<int> bin2dec(vector<int> b2) {
  vector<int> out ((b2.size()/2)+1,0);  
  vector<int> count = {1};

  for(int i = 0; i < b2.size(); i++) {
    if(b2[i]) {
      for(int j = 0; j < count.size(); j++) {
	out[j] += count[j];
	if(out[j] >= 10) {
	  out[j+1] ++;
	  out[j] %= 10;
	}
      }
    }
    // basically just moving up the power of 2 tree
    // mx by two every iteration and add it to the total 
    // if the bit is 1
    count = b10x2(count);
  }

  // trim leading zeros
  while(out.back() == 0 && out.size() > 1)
    out.pop_back();

  return out;
}

// multiply by 2 in base 10
vector<int> b10x2(vector<int> in) {
  vector<int> out = in;
  out.push_back(0);

  // basically just adds two of the same vectors
  for(int i = 0; i < in.size(); i++) {
    out[i] += in[i];
    if(out[i] >= 10) {
      out[i+1] ++;
      out[i] %= 10;
    }
  }

  // trim leading zeros
  while(out.back() == 0 && out.size() > 1)
    out.pop_back();

  return out;
}

// greater than or equal to
bool gtet(vector<int> larger, vector<int> smaller) {
  
  // trim leading zeros to avoid length confusion
  while(!larger.back() && larger.size() > 1)
    larger.pop_back();
  while(!smaller.back() && smaller.size() > 1)
    smaller.pop_back();

  // if they're not equal just return the length comparison
  if(larger.size() != smaller.size())
    return larger.size() > smaller.size();

  // find the first occurrence of the smaller vector
  // having a larger bit
  for(int i = larger.size()-1; i >= 0; i--)
    if(smaller[i] == larger[i])
      continue;
    else if(smaller[i] > larger[i])
      return false;
    else
      return true;
}

// primality test
bool is_prime(vector<int> num) {
  vector<int> three = {1,1};
  vector<int> nm1 = num;

  // since randomly generated int will have 1
  // in MSB and LSB, this subtracts 1, always
  nm1[0] = 0;

  // perform the modular exponentiation test
  vector<int> t = mod_exp_N(three, nm1, num);

  // trim
  while(!t.back() && t.size() > 1)
    t.pop_back();

  // if its one, were good
  if(t.size() == 1 && t[0] == 1)
    return true;

  return false;
}

// modular exponentiation
vector<int> mod_exp_N(vector<int> x, vector<int> y, vector<int> N) {
  // base of 3
  vector<int> z (1,1);

  for(int i = y.size()-1; i >= 0; i--) {
	// the .second just accesses the remainder returned
	// by the division pair
    z = bin_div(bin_mx(z, z), N).second;
    if(y[i])
      z = bin_div(bin_mx(x, z), N).second;
  }

  return z;
}

// random int generator, MSB = LSB = 1
vector<int> n_bit_int(int n) {
  unsigned seed = chrono::system_clock::now().time_since_epoch().count();
  mt19937 rand (seed);
  vector<int> num;
  
  for(int i = 0; i < n; i++)
    num.push_back(rand() % 2);
  num[0] = 1;
  num[n-1] = 1;

  return num;
}

// binary division algorithm, adapted from HW3
// simplified with the new gtet function
pair<vector<int>,vector<int>> bin_div(vector<int> num, vector<int> denom) {
  // boilerplate stuff to avoid dividing by zero or wasting time when
  // the denom is greater than the num
  
  if(!num.size() || (num.size() == 1 && num[0] == 0) )
    return make_pair(num, num);
  if(!denom.size() || (denom.size() == 1 && denom[0] == 0) ) {
    cout << "error: divide by zero" << endl;
    return make_pair(denom, denom);
  }

  if(denom.size() > num.size()) {
    vector<int> zero = {0};
    return make_pair(zero, num);
  }

  vector<int> r;
  vector<int> q (num.size()-denom.size()+1,0);

  // basically just iterates through and
  // preys upon the idea that the next quotient bit can 
  // only be 1 or 0 based on the comparison between current 
  // remainder and the dividend
  for( int i = num.size()-1; i >= 0; i-- ) {
    r.insert(r.begin(), 0);
    
    r[0] = num[i];

    if( gtet(r, denom) ) {
      r = bin_sub(r, denom);
      q[i] = 1;
    }
  }

  // trimmings
  while(!q.back() && q.size() > 1)
    q.pop_back();
  while(!r.back() && r.size() > 1)
    r.pop_back();

  return make_pair(q,r);
}

// binary multiplication, adapted from HW3
vector<int> bin_mx(vector<int> one, vector<int> two) {
  // taking care of zeros
  if(!one.size() )
    return one;
  if(!two.size())
    return two;

  // taking care of the basic cases
  if(one.size() == 1)
    if(one[0] == 0)
      return one;
    else
      return two;
  if(two.size() == 1)
    if(two[0] == 0)
      return two;
    else
      return one;

  // biggest the result can be is size(one) + size(two)
  vector<int> out (one.size() + two.size(), 0);

  // basically we just iterate through one number
  // multiplying it by every digit of the other, right
  // padding to account for orders of magnitude

  while(one.size() >= 1) {
    if(one[0]) {
      for(int i = 0; i < two.size(); i++) {
		out[i] += two[i];
	if(out[i] >= 2) {
	  out[i+1] += out[i] / 2;
	  out[i] %= 2;
	}
      }
    }    
    one.erase(one.begin());
    two.emplace(two.begin(), 0);
  }

  // trimming
  while(!out.back())
    out.pop_back();
  
  return out;
}

// binary subtraction, also adapted and improved
vector<int> bin_sub(vector<int> larger, vector<int> smaller) {
  // all call occurrences account for input validation, so this 
  // tests for equality
  if(gtet(smaller, larger)) {
    vector<int> zero = {0};
    return zero;
  }
  
  vector<int> out;
  bool borrow = false;
  
  for( int i = 0; i < larger.size(); i++ )
    out.push_back(0);
  
  // go through the larger of the two numbers, subtracting
  // and keeping track of a carry
  
  for( int i = 0; i < larger.size(); i++ ) {
    if(borrow) {
      larger[i]--;
      if(larger[i] < 0) {
	borrow = true;
	larger[i] += 2;
      } else {
	borrow = false;
      }
    }
    
    if(i >= smaller.size())
      out[i] = larger[i];
    else
      out[i] = larger[i] - smaller[i];
    
    if( out[i] < 0 ) {
      borrow = true;
      out[i] += 2;
    } 
  }
  
  // trimming
  while(!out.back() && out.size() > 1)
    out.pop_back();
  
  return out;
}

// simple print helper, can print as array or just
// number based on #define ARRAYIFY at top
void print(vector<int> num, bool cr) {
  if(!num.size())
    cout << "[0]";
  for(int i = num.size()-1; i >= 0; i--)
    if(ARRAYIFY)
      cout << (i == num.size()-1 ? "[" : "") << num[i] << (i == 0 ? "]" : ",");
    else
      cout << num[i];
  if(cr)
    cout << endl;
}
