CS315-001
Homework 2
Matt Ruffner

NOTE: the programs use the c++ 11 standard, g++ flag -std=c++0x

* To compile, simple run make.
* There are targets for problem{1-3}, however the default
   target compiles and runs all three sequentially.