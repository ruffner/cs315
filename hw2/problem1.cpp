#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

vector<int> bin2dec2(vector<int> in);
void print(vector<int> in);
void by2dec(vector<int> & in);
void add_one(vector<int> & in);

int main(int argc, char * argv[]) {
  cout << "---------------" << endl;
  cout << "|  PROGRAM 1  |" << endl;
  cout << "---------------" << endl;

  // 1111111111111111111100
  // store bits
  vector<int> one;
  one.push_back(0);
  one.push_back(0);
  for(int i = 0; i < 20; i++)
    one.push_back(1);

  // 100011101
  // store bits
  vector<int> small;
  small.push_back(1); small.push_back(0); small.push_back(0); small.push_back(0);
  small.push_back(1); small.push_back(1); small.push_back(1); small.push_back(0); small.push_back(1);
  
  //1 + 0(x149) + 1 + 0(x149)
  // store bits
  vector<int> big;
  for(int i = 0; i < 149; i++)
     big.push_back(0);
  big.push_back(1);
  for(int i = 0; i < 149; i++)
     big.push_back(0);
  big.push_back(1);
  
  
  print(small); cout << ": "; print(bin2dec2(small)); cout << endl;
  
  print(one); cout << ": "; print(bin2dec2(one)); cout << endl;
  
  print(big); cout << ": "; print(bin2dec2(big));  cout << endl;

  
  return 0;
}

// convenience
void print(vector<int> in) {
  for(int i = in.size()-1; i >= 0; i--)
    cout << in[i];
}

// convenience,  and main algorithm looks
void by2dec(vector<int> & in) {
  int c = 0;

  for(int i = 0; i < in.size(); i++) {
    in[i] = (in[i] * 2) + c;
    c = floor(in[i]/10);
    if(in[i] >= 10) {
      in[i] %= 10;
    }
  }
}

// convenience, and main algorithm looks
void add_one(vector<int> & in) {
  in[0]++;
  for(int i = 0; i < in.size(); i++)
    if(in[i] >= 10) {
      in[i] -= 10;
      in[i+1]++;
    }
}

// main convert algorithm
vector<int> bin2dec2(vector<int> in) {
  int total = 0;
  vector<int> out;
  
  // initialize an output vector of zeros with the required 
  // number of digits based on the input length
  for(int i = 0; i < (int)ceil(in.size()/(log(10)/log(2))); i++)
    out.push_back(0);
    
  for(int i = in.size()-1; i >= 0; i--) {
    // multiply out output by 2
    by2dec(out);
    
    // add one depending on current bit
    if(in[i] == 1)
      add_one(out);
  }
  
  // return out vector
  return out;
}
