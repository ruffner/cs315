#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

vector<int> add_in_b2k(vector<int> large, vector<int> small, int k);
void print(vector<int> in);

int main(int argc, char * argv[]) {
  cout << "---------------" << endl;
  cout << "|  PROGRAM 3  |" << endl;
  cout << "---------------" << endl;
  
  
  // k = 3, 4500000045
  vector<int> base8;
  base8.push_back(4); base8.push_back(5); base8.push_back(0); base8.push_back(0);
  base8.push_back(0); base8.push_back(0); base8.push_back(0); base8.push_back(0);
  base8.push_back(4); base8.push_back(5);
  
  // 0, empty
  vector<int> zero;
  
  // k = 3, 034500005556
  vector<int> base8_2;
  base8_2.push_back(0); base8_2.push_back(3); base8_2.push_back(4); base8_2.push_back(5);
  base8_2.push_back(0); base8_2.push_back(0); base8_2.push_back(0); base8_2.push_back(0); 
  base8_2.push_back(5); base8_2.push_back(5); base8_2.push_back(5); base8_2.push_back(6); 
  
  // k = 3, 553222313202
  vector<int> base8_3;
  base8_3.push_back(5); base8_3.push_back(5); base8_3.push_back(3); base8_3.push_back(2);
  base8_3.push_back(2); base8_3.push_back(2); base8_3.push_back(3); base8_3.push_back(1);
  base8_3.push_back(3); base8_3.push_back(2); base8_3.push_back(0); base8_3.push_back(2); 
  
  // k = 2, 010110001111 
  vector<int> base2;
  base2.push_back(0); base2.push_back(1); base2.push_back(0); base2.push_back(1); base2.push_back(1);
  base2.push_back(0); base2.push_back(0);base2.push_back(0); base2.push_back(1); base2.push_back(1);
  base2.push_back(1); base2.push_back(1);
  
  // k = 2, 110111111 
  vector<int> base2_2;
  base2_2.push_back(1); base2_2.push_back(1); base2_2.push_back(0); base2_2.push_back(1); base2_2.push_back(1);
  base2_2.push_back(1); base2_2.push_back(1); base2_2.push_back(1); base2_2.push_back(1);
  
  // answer display:
  print(base8); cout << " + "; print(zero); cout << ": "; print(add_in_b2k(base8, zero, 3)); cout << endl;
  
  print(base8_2); cout << " + "; print(base8_3); cout << ": "; print(add_in_b2k(base8_3, base8_2, 3)); cout << endl;

  print(base2); cout << " + "; print(base2_2); cout << ": "; print(add_in_b2k(base2, base2_2, 1)); cout << endl;

  
  return 0;
}

// convenience
void print(vector<int> in) {
  if(!in.size()) {
    cout << "0";
    return;
  }
  for(int i = in.size()-1; i >= 0; i--)
    cout << in[i];
}

// the add algorithm, the larger numbered vector first in the arguments
vector<int> add_in_b2k(vector<int> large, vector<int> small, int k) {
  // find our actual base
  int inc = pow(2, k);
  
  vector<int> out = large;
  // adding 2 numbers can only extend the magnitude by 1 at max
  out.push_back(0);
  
  // iterate through the smaller number
  // adding the numbers from it
  for(int i = 0; i < small.size(); i++) {
    out[i] += small[i];
    if(out[i] >= inc) {
      // add over the carry
      out[i+1] += floor(out[i]/inc);
      // use modulo to find the current place value
      out[i] %= inc;
    }
  }
  
  // in the case that the last digit of the smaller
  // number created an undealt with carry, iterate through
  // the last of the output and finish carrying
  for(int i = small.size(); i < out.size(); i++)
    if(out[i] >= inc) {
      out[i+1] += floor(out[i]/inc);
      out[i] %= inc;
    }
  
  
  return out;
}