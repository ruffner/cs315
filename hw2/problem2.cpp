#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

vector<int> twok2bin(vector<int> in, int k);
void print(vector<int> in);

int main(int argc, char * argv[]) {
  cout << "---------------" << endl;
  cout << "|  PROGRAM 2  |" << endl;
  cout << "---------------" << endl;
  
  // base 8, k = 3
  vector<int> base8;
  base8.push_back(7);
  base8.push_back(3);
  base8.push_back(4);
  base8.push_back(5);
  print(base8); cout << ": "; print(twok2bin(base8, 3)); cout << endl;
  
  
  // base 256, k = 8
  vector<int> base256;
  base256.push_back(211);
  base256.push_back(17);
  base256.push_back(128);
  base256.push_back(63);
  print(base256); cout << ": "; print(twok2bin(base256, 8)); cout << endl;
}

// convenience
void print(vector<int> in) {
  for(int i = in.size()-1; i >= 0; i--)
    cout << in[i];
}

// convenience,  and main algorithm looks
// multiplies a vector by a number n, keeping the
// result in binary
void by_n_in_bin(vector<int> & in, int n) {
  int c = 0;

  for(int i = 0; i < in.size(); i++) {
    in[i] = (in[i] * n) + c;
    c = floor(in[i]/2);
    if(in[i] >= 2) {
      in[i] %= 2;
    }
  }
}

// convenience, and main algorithm looks
// simply adds one to a binary vector;
void add_one(vector<int> & in) {    
  in[0]++;
  for(int i = 0; i < in.size(); i++)
    if(in[i] >= 2) {
      in[i] %= 2;
      in[i+1]++;
    }
}

// main algorithm
// converts a vector in base 2^k into a binary vector
vector<int> twok2bin(vector<int> in, int k) {
 
  // if its alredy in binary, return it.
  if(k == 1)
    return in;
  
  int base = pow(2,k);
  
  // calculate how many digits we will need at most and
  // initialize output vector
  vector<int> out;
  for(int i = 0; i < (int)ceil(in.size()*(log(pow(2,k))/log(2))); i++)
    out.push_back(0);
  
  // loop through the digits backwards
  for(int i = in.size()-1; i >= 0; i--) {
    // multiply by our base every time through
    by_n_in_bin(out, base);
    
    // if the digit is non-zero, add that many to
    // the binary output vector.
    if(in[i])
      for(int j = 0; j < in[i]; j++)
        add_one(out);
  }
  
  
  return out;
}